**1.** Preparation
```shell

git clone https://gitlab.com/jim.wen/hypothesis_backend.git
git clone https://gitlab.com/jim.wen/kmass.git

apt install python3.8 python3.8-venv python3-venv

Collecting psycopg2==2.9.9
  Using cached psycopg2-2.9.9.tar.gz (384 kB)
  Preparing metadata (setup.py) ... error
  error: subprocess-exited-with-error

sudo apt-get update
sudo apt-get install libpq-dev


# install requirements
pip install wheel
sudo apt install python3-testresources
python3 -m pip install -r requirements/requirements.txt
```

**1.** Initialization
```shell

# You can keep going only the preparation is ready and no errors, 

# install virtual environment
python3 -m venv venv

# activate virtual environment
source venv/bin/activate

# add the current path into the environment path.
# E.g., the current path is as follows,
# /home/hypothesis/hypothesis_backend, so use the export command and add it into the environment path
# dont forget, otherwise ModuleNotFoundError: No module named 'h'

export PYTHONPATH="/home/hypothesis/hypothesis_backend:$PYTHONPATH"

# make sure there is no service as follows running:
# apache2
# postgresql
# postgresql

# if not, disable it
systemctl stop apache2
systemctl status apache2

systemctl stop postgresql@15-main.service
systemctl disable postgresql@15-main.service

systemctl stop postgresql
systemctl disable postgresql

# check port 5000 whether is occupied
lsof -i -P -n | grep 5000

# docker services
docker network create dbs
docker-compose -f docker-compose.yml up -d
docker ps

# build client js
yarn build

python -m h --dev init

```

**2.** Running
```shell

pserve --reload conf/development-app.ini
pserve conf/development-app.ini

# the screen should display as follow:
(venv) root@kmass-backend:/home/hypothesis/hypothesis_backend# pserve conf/development-app.ini
Starting server in PID 675719.
2023-02-27 23:47:14,293 [675719] [gunicorn.error:INFO] Starting gunicorn 20.1.0
2023-02-27 23:47:14,294 [675719] [gunicorn.error:INFO] Listening at: http://0.0.0.0:5000 (675719)
2023-02-27 23:47:14,294 [675719] [gunicorn.error:INFO] Using worker: sync
2023-02-27 23:47:14,300 [675759] [gunicorn.error:INFO] Booting worker with pid: 675759

```


### Reset database
```shell
python -m h --dev resetdb

# add user for client_url
python -m h --dev user add
python -m h --dev user admin admin

pserve conf/development-app.ini
```

### add OAuth access
http://localhost:5000/users/admin

```shell
python -m h --dev init

deactivate

# build necessary resource
make dev

```

```shell
source venv/bin/activate
# add user
python -m h --dev user add
python -m h --dev user admin admin

make dev
```

**2.** add oauth client
### create oauth client
```
localhost:5000/login
localhost:5000/admin/oauthclients
```

### add `h.client_oauth_id` value to conf/development-app.ini
```ini
# sample
h.client_oauth_id:c704deea-5760-11ed-ab32-0baca72a1be6

Client
{current_scheme}://{current_host}:5000
```

### Setting for PgAdmin
```shell
# user@domain.com
# SuperSecret

docker network ls

docker run -p 80:80 --network dbs -e 'PGADMIN_DEFAULT_EMAIL=user@domain.com' -e 'PGADMIN_DEFAULT_PASSWORD=SuperSecret' -d dpage/pgadmin4

docker ps

docker inspect {progresssql id}

psql -h 0.0.0.0 -p 5432 --username=postgres postgres
```

### Migrate database
```shell
# go the relevant table
# Tools -> import/export Data...
# Storage Manager -> download the file
# go to another pgadmin, select relevant table
# Tools -> import/export Data...
```
### Logo
1. go to the organization table in postgresql
2. rewrite logo column using kmass-logo.svg content
